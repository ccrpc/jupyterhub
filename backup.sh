#!/bin/bash

# Nightly backup of wordpress site.
JUPYTERHUB_DIR=$BACKUP_DIR/jupyterhub

# Create the backup directory if it doesn't exist.
mkdir -p $JUPYTERHUB_DIR

# Remove the oldest backup.
if [ -d "$JUPYTERHUB_DIR/notebook-365" ]; then
  rm -rf $JUPYTERHUB_DIR/notebook-365
fi

# Rotate previous backups.
for i in `seq 0 364`; do
  OLD_FILES=$JUPYTERHUB_DIR/notebook-$(expr 364 - $i)
  NEW_FILES=$JUPYTERHUB_DIR/notebook-$(expr 365 - $i)
  if [ -d $OLD_FILES ]; then
    mv $OLD_FILES $NEW_FILES
  fi
done

# Create a new incremental backup of files.
rsync -aqh --delete \
  --link-dest=$JUPYTERHUB_DIR/notebook-1/ $PWD/var/notebook/ $JUPYTERHUB_DIR/notebook-0/

