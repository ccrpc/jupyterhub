# Configuration file for JupyterHub
import os
import shutil

def format_volume_name(label_template, spawner):
    path = label_template.format(username=spawner.escaped_name)

    # Create user notebooks directory.
    if not os.path.isdir(path):
        os.makedirs(path)
        shutil.chown(path, user=1000, group=1000)
        os.chmod(path, 0o755)

    return path

c = get_config()
c.JupyterHub.base_url = '/jupyter/'
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'

c.DockerSpawner.container_image = os.environ['DOCKER_NOTEBOOK_IMAGE']
spawn_cmd = os.environ.get('DOCKER_SPAWN_CMD', "start-singleuser.sh")
c.DockerSpawner.extra_create_kwargs.update({ 'command': spawn_cmd })

network_name = os.environ['DOCKER_NETWORK_NAME']
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = network_name
c.DockerSpawner.extra_host_config = { 'network_mode': network_name }

notebook_dir = os.environ.get('DOCKER_NOTEBOOK_DIR') or '/home/jovyan/work'
c.DockerSpawner.notebook_dir = notebook_dir
c.DockerSpawner.volumes = {
    '/opt/docker/jupyterhub/var/notebook/{username}': notebook_dir
}
c.DockerSpawner.extra_create_kwargs.update({
    'volume_driver': 'local',
})
c.DockerSpawner.format_volume_name = format_volume_name

c.DockerSpawner.remove_containers = True
c.DockerSpawner.debug = False

# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = 'jupyterhub'
c.JupyterHub.hub_port = 8080

# Authenticate users with LDAP (Active Directory)
c.JupyterHub.authenticator_class = 'ldapauthenticator.LDAPAuthenticator'
c.LDAPAuthenticator.use_ssl = False
c.LDAPAuthenticator.server_address = '192.168.201.141'
c.LDAPAuthenticator.bind_dn_template = 'CCOUNTY\{username}'
c.LDAPAuthenticator.lookup_dn = False
c.LDAPAuthenticator.user_search_base = \
    'OU=Planning,OU=RPC,DC=co,DC=champaign,DC=il,DC=us'
c.LDAPAuthenticator.user_attribute = 'sAMAccountName'
c.LDAPAuthenticator.allowed_groups = []

c.Authenticator.admin_users = ['mjy11187']
